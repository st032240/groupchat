package ru.chat.util;

import ru.chat.admin.MainAdmin;
import ru.chat.admin.SocketThread;

import java.io.IOException;
import java.util.Scanner;

public class SendMessages extends Thread {

    @Override
    public void run() {
        try (Scanner scanner = new Scanner(System.in)) {
            String line;
            while (true) {
                if (scanner.hasNextLine()) {
                    line = scanner.nextLine();
                    for (SocketThread socketThread: MainAdmin.listSockets) {
                        socketThread.sendMessage(line);
                    }
                    if (line.equals("stop")) {
                        for (SocketThread socketThread: MainAdmin.listSockets) {
                            socketThread.getSocket().close();
                        }
                        MainAdmin.socket.close();
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
