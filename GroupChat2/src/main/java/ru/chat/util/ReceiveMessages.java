package ru.chat.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Scanner;

public class ReceiveMessages extends Thread {

    public ReceiveMessages(Socket socket) {
        this.socket = socket;
        start();
    }

    private Socket socket;

    @Override
    public void run() {
        try (InputStream inputStream = socket.getInputStream();
             Scanner scanner = new Scanner(inputStream)) {

            String line;
            while (true) {

                if (scanner.hasNextLine()) {
                    line = scanner.nextLine();
                    if (line.equals("stop")) {
                        socket.close();
                        throw new IOException();
                    }
                    System.out.println(line);
                }
            }
        } catch (IOException e) {
            System.out.println("Chat closed !!!");
        }
    }
}
