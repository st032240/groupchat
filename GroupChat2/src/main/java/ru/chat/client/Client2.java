package ru.chat.client;

import ru.chat.util.ReceiveMessages;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client2 {


    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost",12345)) {

            new ReceiveMessages(socket);
            Scanner scanner = new Scanner(System.in);
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println("Client2 joined");

            while (true) {
                if (scanner.hasNextLine()) {
                    writer.println(scanner.nextLine());
                }
            }


        } catch (IOException e) {
            System.out.println("Chat closed !!!");
        }
    }
}
