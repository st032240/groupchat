package ru.chat.admin;

import ru.chat.util.SendMessages;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

public class MainAdmin {

    public static List<SocketThread> listSockets = new LinkedList<>();

    public static ServerSocket socket;

    public static void main(String[] args) throws IOException {

        try {
            socket = new ServerSocket(12345);
            SendMessages sendMessages = new SendMessages();
            sendMessages.start();

            while(true) {
                Socket clientSocket = socket.accept();
                listSockets.add(new SocketThread(clientSocket));
            }
        } catch (IOException e) {
            System.out.println("Chat closed");

        } finally {
            socket.close();
        }
    }
}
