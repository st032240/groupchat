package ru.chat.admin;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class SocketThread extends Thread {

    private Socket socket;
    private PrintWriter writer;

    public SocketThread(Socket socket) throws IOException {
        this.socket = socket;
        writer = new PrintWriter(socket.getOutputStream(),true);
        start();
    }

    public Socket getSocket() {
        return socket;
    }

    public void sendMessage(String line) {
        writer.println(line);
    }

    @Override
    public void run() {
        try (InputStream inputStream = socket.getInputStream();
             Scanner scanner = new Scanner(inputStream)) {

            String line;
            while (true) {
                if (scanner.hasNextLine()) {
                    line = scanner.nextLine();
                    System.out.println(line);
                    for (SocketThread socketThread: MainAdmin.listSockets) {
                        if (socketThread.socket != this.socket) {
                            socketThread.sendMessage(line);
                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
